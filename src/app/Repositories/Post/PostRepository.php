<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Repositories\Repository;

class PostRepository extends Repository implements PostRepositoryInterface
{
    public function __construct()
    {
        parent::__construct(new Post());
    }

    public function allWithCategories()
    {
        return $this->model->with('categories')->get();
    }
}
