<?php

namespace App\Repositories;

interface RepositoryInterface
{
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @return mixed
     */
    public function show($id);

    /**
     * @param int|null $perPage
     * @param array|string[] $columns
     * @param string $pageName
     * @param int|null $page
     * @return mixed
     */
    public function paginate($perPage, array $columns, string $pageName, $page);
}
