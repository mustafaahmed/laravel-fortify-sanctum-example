<?php

namespace App\Repositories\Category;

use App\Models\Category;
use App\Repositories\Repository;

class CategoryRepository extends Repository implements CategoryRepositoryInterface
{
    public function __construct()
    {
        parent::__construct(new Category());
    }

    public function allWithPosts()
    {
        return $this->model->with('posts')->get();
    }
}
