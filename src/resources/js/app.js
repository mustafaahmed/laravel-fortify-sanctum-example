import Vue from "vue";
import router from "./router";
import App from "./App.vue";
import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:8010/api'
axios.defaults.withCredentials = true

new Vue({
    router,
    render: (h) => h(App),
}).$mount("#app");
